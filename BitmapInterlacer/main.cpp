#include <windows.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "bitmap_image.h"

#include <iostream>
using namespace std;

int main(void)
{
    unsigned int i;
    for(i=0; i<0xFF; i++){
        char filename[16];
        memset(filename,0,sizeof(filename));
        sprintf(filename,"%d.bmp",i);

        // Open  image
        bitmap_image image(filename);

        if (!image)
        {
            printf("Failed to open: %s\n",filename);
        }else{

        // Swap line 0 and line 1
        uint32_t height = image.height();
        uint32_t width  = image.width();
        for (uint32_t y = 0; y < height; y+=2)
        {
            for (uint32_t x = 0; x < width; ++x)
            {
                if(y != height-1){
                    uint8_t red0,green0,blue0;
                    uint8_t red1,green1,blue1;
                    // Store line 0
                    image.get_pixel(x,y,red0,green0,blue0);
                    // Store line 1
                    image.get_pixel(x,y+1,red1,green1,blue1);
                    // Write line 0
                    image.set_pixel(x,y,red1,green1,blue1);
                    // Write line 1
                    image.set_pixel(x,y+1,red0,green0,blue0);
                }
                /*
                if(x != width-1){
                    uint8_t red0,green0,blue0;
                    uint8_t red1,green1,blue1;
                    // Store col 0
                    image.get_pixel(x,y,red0,green0,blue0);
                    // Store col 1
                    image.get_pixel(x+1,y,red1,green1,blue1);
                    // Write col 0
                    image.set_pixel(x,y,red1,green1,blue1);
                    // Write col 1
                    image.set_pixel(x+1,y,red0,green0,blue0);
                }*/
            }
        }

        image.save_image(filename);
        printf("Interlaced: %s\n",filename);
        }
    }

    return 0;
}
